# Umami

[![Umami docs](https://img.shields.io/badge/info-documentation-informational)](https://umami.docs.cern.ch/)  For Umami documentation

[![puma-hep Plotting Tools](https://img.shields.io/badge/info-documentation-informational)](https://umami-hep.github.io/puma/main/index.html)  For puma-hep low level plotting API

[![Singularity Documentation](https://img.shields.io/badge/info-documentation-informational)](https://sylabs.io/guides/3.1/user-guide/cli/singularity_exec.html) For singularity docker usage documentation

[![Apptainer Documentation](https://img.shields.io/badge/info-documentation-informational)](https://sylabs.io/guides/3.1/user-guide/cli/singularity_exec.html) For Apptainer usage documentation. Apptainer is another environment like singularity and docker.

[![UPP Pre-Processing Documentation](https://img.shields.io/badge/info-documentation-informational)](https://umami-hep.github.io/umami-preprocessing/) For a more versatile, time efficient pre-processing, whose files can be directly used in Umami/SALT based training with little to no overhead.

## Installation

You can find the details described in the [umami docs](https://umami.docs.cern.ch/setup/installation/).

More details can be found [here](https://umami.docs.cern.ch/setup/development/).

The instructions below are more optimized for the UChicago Analysis Facility Tier-3 Cluster.
See here: [![Umami docs](https://img.shields.io/badge/info-documentation-informational)](https://maniaclab.uchicago.edu/af-docs/)

### Installation in a nutshell

#### Preparing the environment

```
cd <Umami top level dir>
setupATLAS -3 -q

# Use the python ver below only if needed
# lsetup "python 3.9.18-x86_64-centos7"
```
#### Setting up Singularity/Apptainer

```
source exportsingcache.sh

source singstartup.sh
# Choose option 1 for CPU image and 2 for GPU image
```
#### Installing all required dependencies

```
python3 -m pip install -e .

source run_setup.sh
```
If `upp` is not available in `<working_dir>/umami/.local/lib/python3.8/site-packages/` , copy it over to that folder by downloading [UPP](https://github.com/umami-hep/umami-preprocessing/).
Same goes for `scipy` and `dotmap` .

## Preprocessing

The ntuples need to be preprocessed following the [Preprocessing Instructions](https://umami-docs.web.cern.ch/preprocessing/Overview/).

### Pre-Processing in a nutshell (Umami based)

To pre-process the ntuples, simply do the following:

```
source execute_preprocess_DL1d.sh
```
You would need to uncomment all lines in the `execute_preprocess_DIPS.sh` script if you're pre-processing for the first time. Make appropriate changes to `examples/preprocessing/<script>.yaml` files.

### Pre-Processing in a nutshell (UPP based)

To pre-process the ntuples, simply do the following:

```
preprocess --config single-b.yaml --split=all
```

You would need to make appropriate changes to `single-b.yaml` file.

## Training

If you want to train or evaluate DIPS/DL1d please follow the [Training Instructions](https://umami-docs.web.cern.ch/trainings/Overview/).

### Training in a nutshell

To train the ntuples, simply do the following:

```
source execute_train_DL1d.sh
```

### Training on HTCondor

To train the ntuples on HTC, simply do the following:

```
condor_submit singtrainjob.sub
condor_analyze -q
condor_ssh_to_job <JOB-ID-SHORT> <command like ls or cat _condor_stdout>
```

You would need to uncomment all lines in the `execute_train_DIPS.sh` script if you're training for the first time. Do make appropriate changes to `examples/preprocessing/<script>.yaml` & `examples/training/<script>.yaml` files.

## Conversion to lwtnn

Like any great model you make for people to use, you would wanna convert it to an lwtnn model. Fret not, the instructions lie below.

```
python3 scripts/create_lwtnn_vardict.py -s dips_lr_0.001_bs_15000_epoch_200_nTrainJets_300M/metadata/PFlow-scale_dict.json -v umami/configs/Dips_Variables_R22.yaml -t DIPS -n tracks_r22default_sd0sort --tracks_name tracks

python3 scripts/conv_lwtnn_model.py -m dips_lr_0.001_bs_15000_epoch_200_nTrainJets_300M/model_files/model_epoch200.h5

#cd ../
echo $PWD

REPO_URL="https://github.com/lwtnn/lwtnn.git"
DIR_NAME="lwtnn"

if [ ! -d "$DIR_NAME" ]; then
    # Directory does not exist, so clone the repository
    git clone "$REPO_URL" "$DIR_NAME"
else
    # Directory exists
    echo "Directory '$DIR_NAME' already exists, so, not cloning!!"
fi

# Add "ptau" inside labels section in lwtnn_vars.json and then run

python3 lwtnn/converters/kerasfunc2json.py umami/architecture-lwtnn_model.json umami/weights-lwtnn_model.h5 umami/lwtnn_vars.json > umami/FINAL-DIPS-tau-new-arch-model.json
```
## Tutorial for Umami

At the [FTAG Workshop in 2022 in Amsterdam](https://indico.cern.ch/event/1193206/timetable/?view=standard), we gave a tutorial how to work with Umami. You can find the slides together with a recording of the talk [here](https://indico.cern.ch/event/1193206/timetable/?view=standard#b-477082-day-3-afternoon-tutor). The corresponding step-by-step tutorial can be found in the FTAG docs webpage [here](https://ftag.docs.cern.ch/software/tutorials/tutorial-umami/).

