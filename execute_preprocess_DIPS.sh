## DIPS PREPROCESSING CONFIG
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample training_ttbar_bjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample training_ttbar_cjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample training_ttbar_ujets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample training_ttbar_taujets  --prepare

preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample training_zprime_bjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample training_zprime_cjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample training_zprime_ujets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample training_zprime_taujets  --prepare

preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample validation_ttbar_bjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample validation_ttbar_cjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample validation_ttbar_ujets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample validation_ttbar_taujets  --prepare

preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample validation_zprime_bjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample validation_zprime_cjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample validation_zprime_ujets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample validation_zprime_taujets  --prepare

preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample validation_ttbar  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample validation_zprime  --prepare


preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample testing_ttbar  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --sample testing_zprime  --prepare

echo "resampling"
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --resampling
echo "resampling hybrid validation"
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml --resampling --hybrid_validation
echo "calculating scaling"
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --scaling
echo "writing to h5"
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --write
echo "writing to tfrecords since tfrecords is faster"
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml  --write --to_records
echo "writing hybrid validation"
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-taus.yaml --write --hybrid_validation
