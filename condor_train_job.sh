#!/bin/bash

#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
#setupATLAS -3 -q
#lsetup "python 3.9.18-x86_64-centos7"
export APPTAINER_CACHEDIR=/home/sgoswami/singularity
export APPTAINER_TMPDIR=/home/sgoswami/tmp/singularity
#docker --verbose -d exec --nv  --bind /home/sgoswami/ --bind /cvmfs --bind /home/sgoswami --home $PWD /home/sgoswami/dockimage/umami_base_gpu.img /bin/bash
INSTDIR=python_install
rm -rf ${INSTDIR}
mkdir ${INSTDIR}
export PYTHONPATH=${PWD}:${PWD}/${INSTDIR}:${PYTHONPATH}
python3 -m pip install --prefix ${INSTDIR} -e .
export PATH=${PWD}/${INSTDIR}/bin:$PATH
echo "CUDA_VISIBLE_DEVICES:" ${CUDA_VISIBLE_DEVICES}
echo "CUDA_VISIBLE_DEVICES:" `nvidia-smi -L`
train.py -c examples/training/Dips-PFlow-Training-config.yaml -e 200 2>&1 
#| tee -a dipstrainlog.txt
