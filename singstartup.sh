DIR="/home/sgoswami/dockimage"
if [ ! -d $DIR ]; then
   mkdir /home/sgoswami/dockimage
fi

DIR1="/home/sgoswami/singularity"
if [ ! -d $DIR1 ]; then
   mkdir /home/sgoswami/singularity
fi
#GPU --nvccli
PS3="Select the run mode: "
select opt in cpu gpu; do
        case $opt in
                cpu)
                        imgcpu="/home/sgoswami/dockimage/umami_base_cpu.img"
                        if [ ! -e $imgcpu ]; then
                                singularity pull /home/sgoswami/dockimage/umami_base_cpu.img docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/algorithms/umami/umamibase:latest
                        fi
                        apptainer --verbose -d exec --bind /home/sgoswami/ --bind /cvmfs --bind /data --home $PWD /home/sgoswami/dockimage/umami_base_cpu.img /bin/bash
                        ;;
                gpu)
                        imggpu="/home/sgoswami/umami_base_gpu.img"
                        if [ ! -e $imggpu ]; then
                                singularity pull /home/sgoswami/dockimage/umami_base_gpu.img docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/algorithms/umami/umamibase-plus:latest-gpu
                        fi
                        apptainer --verbose -d exec --nv --bind /home/sgoswami/ --bind /cvmfs --bind /data --home $PWD /home/sgoswami/dockimage/umami_base_gpu.img /bin/bash
                        ;;
        esac
done