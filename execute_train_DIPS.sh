## DIPS TRAINING CONFIG
train.py -c examples/training/Dips-PFlow-Training-config.yaml --prepare

train.py -c examples/training/Dips-PFlow-Training-config.yaml -e 200

plotting_epoch_performance.py -c examples/training/Dips-PFlow-Training-config.yaml  --recalculate

plotting_epoch_performance.py -c examples/training/Dips-PFlow-Training-config.yaml

evaluate_model.py -c examples/training/Dips-PFlow-Training-config.yaml -e 200

##ttbar
plotting_umami.py -c examples/plotting_umami_config_dips_ttb.yaml -o dips_eval_plots_ttb -f png

##zpext
plotting_umami.py -c examples/plotting_umami_config_dips_zp.yaml -o dips_eval_plots_zp -f png

##plotting ttbar and zpext light-rejection vs c-rejection
python3 plotrej.py

