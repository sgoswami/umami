#Set up Umami and keep pwd at the training_scripts folder level
#1 for std, 2 for lrt
plot_input_variables.py -v -c examples/DIPS/varplotconfigs/jetconfllp.yaml --jets -f png
#plot_input_variables.py -v -c examples/DIPS/varplotconfigs/jetconf1.yaml --jets -f png
#plot_input_variables.py -v -c examples/DIPS/varplotconfigs/jetconf2.yaml --jets -f png

plot_input_variables.py -v -c examples/DIPS/varplotconfigs/ntrackconfllp.yaml --tracks -f png
#plot_input_variables.py -v -c examples/DIPS/varplotconfigs/ntrackconf1.yaml --tracks -f png
#plot_input_variables.py -v -c examples/DIPS/varplotconfigs/ntrackconf2.yaml --tracks -f png

plot_input_variables.py -v -c examples/DIPS/varplotconfigs/trackconfllp.yaml --tracks -f png
#plot_input_variables.py -v -c examples/DIPS/varplotconfigs/trackconf1.yaml --tracks -f png
#plot_input_variables.py -v -c examples/DIPS/varplotconfigs/trackconf2.yaml --tracks -f png
