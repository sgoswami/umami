#python3 scripts/create_lwtnn_vardict.py -s dips_lr_0.001_bs_15000_epoch_200_nTrainJets_300M/metadata/PFlow-scale_dict.json -v umami/configs/Dips_Variables_R22.yaml -t DIPS -n tracks_r22default_sd0sort --tracks_name tracks

python3 scripts/create_lwtnn_vardict.py -s dl1d_lr_0.001_bs_15000_epoch_200_nTrainJets_300M/metadata/PFlow-scale_dict.json -v umami/configs/DL1d_Variables_R22.yaml -t DL1

#python3 scripts/conv_lwtnn_model.py -m dips_lr_0.001_bs_15000_epoch_200_nTrainJets_300M/model_files/model_epoch200.h5

python3 scripts/conv_lwtnn_model.py -m dl1d_lr_0.001_bs_15000_epoch_200_nTrainJets_300M/model_files/model_epoch200.h5

#cd ../
echo $PWD

REPO_URL="https://github.com/lwtnn/lwtnn.git"
DIR_NAME="lwtnn"

if [ ! -d "$DIR_NAME" ]; then
    # Directory does not exist, so clone the repository
    git clone "$REPO_URL" "$DIR_NAME"
else
    # Directory exists
    echo "Directory '$DIR_NAME' already exists, so, not cloning!!"
fi

# Add "ptau" inside labels section in lwtnn_vars.json and then run

#python3 lwtnn/converters/kerasfunc2json.py umami/architecture-lwtnn_model.json umami/weights-lwtnn_model.h5 umami/lwtnn_vars.json > umami/FINAL-DIPS-tau-new-arch-model.json
python3 lwtnn/converters/kerasfunc2json.py umami/architecture-lwtnn_model.json umami/weights-lwtnn_model.h5 umami/lwtnn_vars.json > umami/FINAL-DL1d-tau-new-arch-model.json
