## DL1d PREPROCESSING CONFIG
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample training_ttbar_bjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample training_ttbar_cjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample training_ttbar_ujets  --prepare

preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample training_zprime_bjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample training_zprime_cjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample training_zprime_ujets  --prepare

preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample validation_ttbar_bjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample validation_ttbar_cjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample validation_ttbar_ujets  --prepare

preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample validation_zprime_bjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample validation_zprime_cjets  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample validation_zprime_ujets  --prepare

preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample validation_ttbar  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample validation_zprime  --prepare


preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample testing_ttbar  --prepare
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --sample testing_zprime  --prepare

echo "resampling"
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --resampling
echo "resampling hybrid validation"
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml --resampling --hybrid_validation
echo "calculating scaling"
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --scaling
echo "writing to h5"
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --write
echo "writing to tfrecords since tfrecords is faster"
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml  --to_records
echo "writing hybrid validation"
preprocessing.py -c examples/preprocessing/PFlow-Preprocessing-DL1d.yaml --write --hybrid_validation
