## DL1d TRAINING CONFIG
train.py -c examples/training/DL1d-PFlow-Training-config.yaml --prepare

train.py -c examples/training/DL1d-PFlow-Training-config.yaml -e 200

plotting_epoch_performance.py -c examples/training/DL1d-PFlow-Training-config.yaml  --recalculate

plotting_epoch_performance.py -c examples/training/DL1d-PFlow-Training-config.yaml

evaluate_model.py -c examples/training/DL1d-PFlow-Training-config.yaml -e 200
#evaluate_model.py -c examples/training/DL1d-PFlow-Training-config.yaml -e 200 -s shapley

##ttbar
plotting_umami.py -c examples/plotting_umami_config_dl1d_ttb.yaml -o DL1d_eval_plots_ttb -f png

##zpext
plotting_umami.py -c examples/plotting_umami_config_dl1d_zp.yaml -o DL1d_eval_plots_zp -f png

##plotting ttbar and zpext light-rejection vs c-rejection
python3 plotrej.py

