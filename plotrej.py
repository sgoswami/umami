from __future__ import annotations

import h5py
import numpy as np
import pandas as pd

import numpy as np

from puma import Line2D, Line2DPlot
from puma.metrics import calc_rej
from puma.utils import get_dummy_2_taggers, logger

sig_index = [0,1,2,3]
key_index = [0,1]

def rejplotfn(sigindex: int, keyindex: int):
    file_path = "dl1d_lr_0.001_bs_15000_epoch_200_nTrainJets_300M/results/results-200.h5"
    keystring = ['ttbar_r22','zpext_r22']
    # Try to read the entire DataFrame first
    try:
        df = pd.read_hdf(file_path, key= keystring[keyindex], start=0, stop=300000)
    except KeyError as e:
        print(f"Error reading the file: {e}")
        exit()

    # Check if all specified columns are present
    columns_to_read = [
        "dl1_pb",
        "dl1_pc",
        "dl1_pu",
        "DIPS20231205_pb",
        "DIPS20231205_pc",
        "DIPS20231205_pu",
        "DL1r20210824r22_pb",
        "DL1r20210824r22_pc",
        "DL1r20210824r22_pu",
        "DL1dv01_pb",
        "DL1dv01_pc",
        "DL1dv01_pu",
        "HadronConeExclTruthLabelID"
    ]

    missing_columns = [col for col in columns_to_read if col not in df.columns]
    if missing_columns:
        print(f"Missing columns in the DataFrame: {missing_columns}")
    else:
        # Select only the required columns
        df = df[columns_to_read]
        print(df)

    is_u = df["HadronConeExclTruthLabelID"] == 0
    print(is_u)
    is_c = df["HadronConeExclTruthLabelID"] == 4
    is_b = df["HadronConeExclTruthLabelID"] == 5

    fc_values = np.linspace(0.0, 1.0, 101)
    sig_eff = [0.60,0.70,0.77,0.85]

    dl1d_scores = df[["dl1_pu", "dl1_pc", "dl1_pb"]].values
    dl1dv01_scores = df[["DL1dv01_pu", "DL1dv01_pc", "DL1dv01_pb"]].values
    print(dl1d_scores)
    dips2_scores = df[["DIPS20231205_pu", "DIPS20231205_pc", "DIPS20231205_pb"]].values
    dl1r_scores = df[["DL1r20210824r22_pu", "DL1r20210824r22_pc", "DL1r20210824r22_pb"]].values

    def calc_rejs(arr, fc_value: float):
        """Tagger efficiency for fixed working point.

        Parameters
        ----------
        fc_value : float
            Value for the charm fraction used in discriminant calculation.

        Returns
        -------
        tuple
            Tuple of shape (, 3) containing (fc_value, ujets_eff, cjets_eff)
        """
        #disc = arr[:, 2] / (fc_value * arr[:, 1] + (1 - fc_value- 0.2) * arr[:, 0] + 0.2 * arr[:, 3] )
        disc = arr[:, 2] / (fc_value * arr[:, 1] + (1 - fc_value) * arr[:, 0] )
        ujets_rej = calc_rej(disc[is_b], disc[is_u], sig_eff[sigindex])
        cjets_rej = calc_rej(disc[is_b], disc[is_c], sig_eff[sigindex])

        return [fc_value, ujets_rej, cjets_rej]

    def calc_rejs1(arr, fc_value: float):
        """Tagger efficiency for fixed working point.

        Parameters
        ----------
        fc_value : float
            Value for the charm fraction used in discriminant calculation.

        Returns
        -------
        tuple
            Tuple of shape (, 3) containing (fc_value, ujets_eff, cjets_eff)
        """
        disc = arr[:, 2] / (fc_value * arr[:, 1] + (1 - fc_value) * arr[:, 0])
        ujets_rej = calc_rej(disc[is_b], disc[is_u], sig_eff[sigindex])
        cjets_rej = calc_rej(disc[is_b], disc[is_c], sig_eff[sigindex])

        return [fc_value, ujets_rej, cjets_rej]

    rej_results_dl1d  = np.array(list(map(lambda fc_value: calc_rejs(dl1d_scores, fc_value), fc_values)))
    print(rej_results_dl1d)
    rej_results_dl1dv01  = np.array(list(map(lambda fc_value: calc_rejs(dl1dv01_scores, fc_value), fc_values)))
    rej_results_dips2 = np.array(list(map(lambda fc_value: calc_rejs1(dips2_scores, fc_value), fc_values)))
    rej_results_dl1r  = np.array(list(map(lambda fc_value: calc_rejs1(dl1r_scores, fc_value), fc_values)))

    x_values_dl1d = rej_results_dl1d[:, 2]
    y_values_dl1d = rej_results_dl1d[:, 1]

    x_values_dl1dv01 = rej_results_dl1dv01[:, 2]
    y_values_dl1dv01 = rej_results_dl1dv01[:, 1]

    x_values_dips2 = rej_results_dips2[:, 2]
    y_values_dips2 = rej_results_dips2[:, 1]

    x_values_dl1r = rej_results_dl1r[:, 2]
    y_values_dl1r = rej_results_dl1r[:, 1]

    # Now init a fraction scan plot
    frac_plot = Line2DPlot()

    frac_plot.add(
        Line2D(
            x_values=x_values_dl1d,
            y_values=y_values_dl1d,
            label="DL1d"+str(keystring[keyindex])+"\nWP "+str(sig_eff[sigindex]),
            colour="r",
            linestyle="-",
        )
    )

    # Adding labels
    frac_plot.ylabel = "Light-flavour jets rejection"
    frac_plot.xlabel = "$c$-jets rejection"

    # Draw and save the plot
    frac_plot.draw()
    frac_plot.savefig("FractionScanPlot_rej_rej_DL1d_"+str(keystring[keyindex])+str("_WP"+str(int(100*sig_eff[sigindex])))+".png")

for keyval in key_index:
    for sigval in sig_index:
        rejplotfn(sigval, keyval)


